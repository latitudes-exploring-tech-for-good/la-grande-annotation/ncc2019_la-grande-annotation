# La grande annotation

+ *Thème :* grand débat, citoyenneté.
+ *Phase d'avancement actuelle :* en production.
+ *Compétences associés :* data science, developpement web, design.

##### #0 | Résultats obtenus lors de la Nuit du Code Citoyen
Nous avions touché un public plutôt initié aux enjeux techniques liés à l'IA et à ses limites. Les médias communiquent relativement peu sur les méthodes qui vont être employées pour l'analyse officielle, ni sur les méthodes qu'ils emploient eux-mêmes pour effectuer leurs premiers graphiques.

Les participants à la Nuit du Code nous ont convaincus de l'importance de faire de grandeannotation.fr non seulement un site d'annotation, mais aussi un site de pédagogie sur ce que l'IA peut et ne peut pas faire en 2019.

Pour cela, nous avons entièrement revu le parcours utilisateur sur le site. Auparavant, celui-ci était tourné vers l'annotation directement, sans préambule. Désormais, nous avons une maquette (Sketch) pour une initiation pédagogique et intuitive, basée sur des exemples concrets.

Les participants nous ont aussi aidé à améliorer le parcours de lecture.

[Le code source](https://github.com/oceanBigOne/granddebat).


##### #1 | Présentation de la Grande Annotation
+ Dans le cadre du Grand débat national, le Gouvernement a lancé le 22 janvier 2019 [granddebat.fr](https://granddebat.fr/), un site internet permettant aux citoyens de s’exprimer sur quatre thèmes déclinés en près de 100 questions.
+ La lecture de nombreuses contributions nous a convaincus que l’intelligence artificielle seule ne parviendrait pas à restituer fidèlement les idées, opinions et sentiments exprimés par ceux qui ont participé au débat.
+ Nous sommes aussi convaincus que les citoyens peuvent collectivement réaliser une synthèse de ce débat en adoptant une démarche transparente et ouverte.
+ Nous avons donc lancé une plateforme au début du mois de février pour permettre à tous les internautes de lire et d'annoter les contributions rédigées par d'autres aux questions officielles du grand débat. 

Actuellement, près de 800 personnes ont annoté plus de 50 000 textes. 

En savoir plus : [grandeannotation.fr](https://grandeannotation.fr/).

##### #2 | Problématique : créer des restitutions pertinentes des contributions au grand débat
Plusieurs défis sont proposés selon vos compétences et appétences sur le sujet.

##### #3 | Les défis proposés
Concevoir un mode de restitution sincère des enseignements tirés de l'annotation. La restitution pourra prendre plusieurs formes : graphiques, schémas, textes, visualisations interactives, navigation dans les résultats, etc. La difficulté principale est qu'il ne s'agit ni d'une élection, ni d'un sondage. Les modes de restitutions habituels (pourcentages en premier lieu) sont donc relativement inadaptés.

##### #4 | Livrables
Tout type de profil et de participation est possible au sein de ce projet. Voici quelques exemples :
+ **Experts de la données** : proposer des algorithmes d'analyse sur des axes variés (croisements de questions, géographiques, analyse de texte, de vocabulaire, de sentiment) 
+ **Développeurs** :  le coeur de la plateforme est écrit en PHP (Laravel) avec un peu de JavaScript (Vue.JS) et toutes les améliorations d'expérience utilisateur sont les bienvenues, en particulier pour des modules de restitutions interactives
+ **Designers et graphistes** : rendre intelligible les résultats mais aussi toute la démarche, la méthodologie (d'où viennent les données, comment marche le processus d'annotation avec lectures multiples et consensus majoritaire)
+ **Curieux** : utiliser la plateforme pour lire et annoter, faire des retours d'expérience sur les améliorations à apporter à la plateforme, formulation de synthèses textuelles.

Le contenu exact des livrables est à définir avec les participants selon leurs expertises, leurs envies, et les idées qui surgiront des données. Néanmoins, voici quelques exemples de livrables possibles :

+ Une analyse du niveau de langue employé dans les contributions et une comparaison avec des corpus dont on connaît la composition sociologique pour tenter de déterminer, à partir du texte, le ou les profils des contributeurs au grand débat.
+ Une illustration des difficultés à utiliser les techniques de type NLP pour comprendre le sens de propositions rédigées dans des champs entièrement libres : quel taux de rappel ou de précision sont atteignables étant donné l'état de l'art pour prédire les catégories sur les textes non annotés à partir de la partie annotée du corpus ?
+ Une application pour explorer facilement les données et contributions de granddebat.fr, à destination des journalistes, des chercheurs, des fact checkers, des citoyens. Qu'est-ce qui s'est dit sur un thème ?
+ Une infographie expliquant le processus d'annotation et la démarche de la plateforme grandeannotation.fr : lecture multiples aléatoires, comparaison des annotations des différents lecteurs, ré-itération, critère d'arrêt.
+ Une infographie restituant les enseignements des annotations sur des thèmes ou des questions choisies : arbres de propositions les plus fréquentes, propositions croisées. Le défi est de restituer sans utiliser trop de chiffres car la population des contributeurs n'est sans doute pas représentative de l'ensemble de la population.
+ Une ou des notes textuelles synthétisant les propositions issues des contributions sur un ou plusieurs sujets.

##### #5 | Ressources à disposition pour résoudre le défi
Vous aurez accès à :
+ La plateforme elle-même, pour tester l'expérience utilisateur et se familiariser avec le mécanisme.
+ Le [code source](https://github.com/fm89/granddebat) de la plateforme.
+ Les jeux de données brutes issues du Grand Débat National depuis la plateforme officielle [granddebat.fr](https://granddebat.fr), le dernier export disponible actuellement date du 3 mars (et il y aura peut-être un export plus récent).
+ L'ensemble des [annotations brutes](https://grandeannotation.fr/data) réalisées sur la plateforme Grande Annotation (identifiant de l'annotateur, numéro de la contribution annotée, numéro de la question, libellé de la catégorie). 
+ Des scripts de post-traitement, permettant de détecter ou d'éliminer les annotateurs parasites (ils sont peu nombreux), les erreurs ponctuelles, et les annotations faisant consensus.
+ D'autres exports à la demande pourront être réalisés depuis la base de données en production en temps réel.

##### #6 | Code de conduite et philosophie du hackathon
Lors de la conception du hackathon, Latitudes a voulu prendre des partis-pris afin de rendre celui-ci particulier. 

Il s’agit d’éléments que nous souhaitons incarner collectivement avec vous tout au long des 24h :
+ La force du collectif pour faire émerger des solutions adaptées aux porteur.se.s de projets, notamment via la session de peer-learning ;
+ Une attention portée à la conception rapide, au test et à l’itération qui doivent permettre aux porteur.se.s de projets d’avoir accès aux solutions les plus abouties possibles ;
+ Le transfert de méthodes et postures, qui pourront être appliquées et transmises aux équipes par les animateur.trice.s, les mentors ET les autres équipes ;
+ L’engagement sur des solutions ouvertes, facilement utilisables par les porteur.se.s de projets, et qui vous permettront de continuer à contribuer à l’issue du hackathon si vous le souhaitez, ou de permettre à d’autres personnes de le faire.

##### #7 | Points de contact lors du hackathon
+ Marion : Data-scientist et passionnée de politique publique.
+ Frédéric : Développeur de la plateforme et mathématicien.
+ Margot : bénévole Latitudes, et en charge de la préparation du défi pour la Grande Annotation.